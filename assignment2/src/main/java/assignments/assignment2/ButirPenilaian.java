package assignments.assignment2;

public class ButirPenilaian {
    private static final int PENALTI_KETERLAMBATAN = 20;
    private double nilai;
    private boolean terlambat;
    
    public ButirPenilaian(double nilai,  boolean terlambat) {
        //constructor untuk ButirPenilaian.
        this.nilai = nilai;
        this.terlambat = terlambat; 
        setNilai(this.nilai);//nilai akan diubah menjadi 0 jika nilai yang dimasukkan di glasDos negatif
    }

    public double getNilai() {
        // kembalikan nilai yang sudah disesuaikan dengan keterlambatan.
        if (terlambat) {
           setNilai(this.nilai - (PENALTI_KETERLAMBATAN/100.00)*this.nilai); 
           terlambat = false;
        }
        return this.nilai;
    }
    public void setNilai(double nilai) {
        // Mengubah nilai.
            if (nilai < 0){
                this.nilai = 0;
            }
            else{
                this.nilai = nilai; 
            }
    }

    @Override
    public String toString() {
        // Mengembalikan representasi String dari ButirPenilaian.
        if (terlambat){
            return String.format("%.2f (T)",getNilai());
        }
        else{
            return String.format("%.2f",getNilai());
        }
        
    }
}
