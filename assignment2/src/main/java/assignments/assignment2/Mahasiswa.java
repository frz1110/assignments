package assignments.assignment2;

public class Mahasiswa implements Comparable<Mahasiswa> {
    private String npm;
    private String nama;
    private KomponenPenilaian[] komponenPenilaian;

    public Mahasiswa(String npm, String nama, KomponenPenilaian[] komponenPenilaian) {
        // constructor untuk Mahasiswa.
        // komponenPenilaian merupakan skema penilaian yang didapat dari GlasDOS.
        this.npm = npm;
        this.nama = nama;
        this.komponenPenilaian = komponenPenilaian;
    }

    public KomponenPenilaian getKomponenPenilaian(String namaKomponen) {
        // return KomponenPenilaian yang bernama namaKomponen.
        // jika tidak ada, return null.
        for (KomponenPenilaian e:this.komponenPenilaian){
            if (e.getNama().equalsIgnoreCase(namaKomponen)){
                return e;
            } 
        }
        return null;
    }

    public String getNpm() {
        // method getter untuk NPM mahasiswa.
        return this.npm;
    }

    /**
     * Mengembalikan huruf berdasarkan nilai yang diberikan.
     * @param nilaiAkhir nilai untuk dicari hurufnya.
     * @return huruf dari nilai.
     */
    private static String getHuruf(double nilai) {
        return nilai >= 85 ? "A" :
            nilai >= 80 ? "A-" :
            nilai >= 75 ? "B+" :
            nilai >= 70 ? "B" :
            nilai >= 65 ? "B-" :
            nilai >= 60 ? "C+" :
            nilai >= 55 ? "C" :
            nilai >= 40 ? "D" : "E";
    }

    /**
     * Mengembalikan status kelulusan berdasarkan nilaiAkhir yang diberikan.
     * @param nilaiAkhir nilai akhir mahasiswa.
     * @return status kelulusan (LULUS/TIDAK LULUS).
     */
    private static String getKelulusan(double nilaiAkhir) {
        return nilaiAkhir >= 55 ? "LULUS" : "TIDAK LULUS";
    }

    public double nilaiTotal(KomponenPenilaian[] komponen){
        // Menghitung nilai akhir Mahasiswa.
        // Menambah semua nilai(rerata yang sudah dikalikan bobot) pada komponen penilaian.
        double nilaiTotal = 0;
        for (KomponenPenilaian e :komponenPenilaian){
            nilaiTotal += e.getNilai();
        }
        return nilaiTotal;
    }

    public String rekap() {
        // Rekap nilai seorang mahasiswa.
        String rekapan = "";
        for (KomponenPenilaian e :komponenPenilaian){
            rekapan += e + "\n";
        }
        double nilai = nilaiTotal(komponenPenilaian);

        return rekapan + "Nilai Akhir:" + String.format("%.2f",nilai) + "\n" + "Huruf: " + getHuruf(nilai)
                + "\n" + getKelulusan(nilai) + "\n";
    }

    public String toString() {
        // kembalikan representasi String dari Mahasiswa.
        return this.npm + " - " + this.nama;
    }

    public String getDetail() {
        // mencetak detail nilai dari Mahasiswa.
        // Memanfaatkan method getDetail() dari class komponen penilaian.
        for (KomponenPenilaian e :komponenPenilaian){
            System.out.println(e.getDetail()); 
            System.out.println(); 
        }
        
        double nilai = nilaiTotal(komponenPenilaian);
        System.out.println(); 

        return "Nilai Akhir:" + String.format("%.2f",nilai) + "\n" + "Huruf: " + getHuruf(nilai)
        + "\n" + getKelulusan(nilai);
    }

    @Override
    public int compareTo(Mahasiswa other) {
        // membandingkan seorang mahasiswa dengan mahasiswa lainnya berdasarkan npm.
        return getNpm().compareTo(other.getNpm());
    }
}
