package assignments.assignment2;

import java.util.Arrays;

public class KomponenPenilaian {
    private String nama;
    private ButirPenilaian[] butirPenilaian;
    private int bobot;

    public KomponenPenilaian(String nama, int banyakButirPenilaian, int bobot) {
        // constructor untuk KomponenPenilaian.
        this.nama = nama;
        this.bobot = bobot;
        this.butirPenilaian = new ButirPenilaian[banyakButirPenilaian];
    }

    /**
     * Membuat objek KomponenPenilaian baru berdasarkan bentuk KomponenPenilaian templat.
     * @param templat templat KomponenPenilaian.
     */
    KomponenPenilaian(KomponenPenilaian templat) {
        this(templat.nama, templat.butirPenilaian.length, templat.bobot);
    }

    /**
     * Mengembalikan salinan skema penilaian berdasarkan templat yang diberikan.
     * @param templat templat skema penilaian sebagai sumber.
     * @return objek baru yang menyerupai templat.
     */
    public static KomponenPenilaian[] salinTemplat(KomponenPenilaian[] templat) {
        KomponenPenilaian[] salinan = new KomponenPenilaian[templat.length];
        for (int i = 0; i < salinan.length; i++) {
            salinan[i] = new KomponenPenilaian(templat[i]);
        }
        return salinan;
    }

    public void masukkanButirPenilaian(int idx, ButirPenilaian butir) {
        // Memasukkan butir ke butirPenilaian pada index ke-idx.
        butirPenilaian[idx] = butir;
    }

    public String getNama() {
        // kembalikan nama KomponenPenilaian.
        return this.nama;
    }

    public ButirPenilaian getElemen(int idx) {
        // kembalikan nama KomponenPenilaian.
        return this.butirPenilaian[idx];
    }
    

    public double getRerata() {
        // Mencari rata-rata pada butirPenilaian.
        double jumlahButir = 0;
        int banyakButir =0;
        for ( ButirPenilaian butir:butirPenilaian){
            if (butir != null){
                jumlahButir += butir.getNilai();
                banyakButir += 1;
            }
        }
        if ( banyakButir != 0){
           return jumlahButir/banyakButir;
        }
        else {
            return 0;
        }
    }

    public double getNilai() {
        // return rerata yang sudah dikalikan dengan bobot.
        return (getRerata()*bobot)/100;
    }

    public String getDetail() {
        // detail KomponenPenilaian.
        System.out.println("~~~ " + this.nama + "("+this.bobot+"%"+")" + " ~~~");
        if(butirPenilaian.length > 1){
            for (ButirPenilaian butir:butirPenilaian){
                if (butir != null){
                    System.out.println(this.nama + " " + (Arrays.asList(butirPenilaian).indexOf(butir) + 1) + " : " +butir);
                }
            }
            System.out.println("Rerata "+ ":" + String.format("%.2f",getRerata()));
        }
        else if (butirPenilaian.length == 1){
            for (ButirPenilaian butir:butirPenilaian){
                if (butir != null){
                    System.out.println(this.nama + " : " +butir);
                }
            }
        }
        return "Kontribusi Nilai Akhir: " + String.format("%.2f",getNilai());
    }

    @Override
    public String toString() {
        // Mengembalikan representasi String sebuah KomponenPenilaian.
        return "Rerata "+ this.nama + ": " + String.format("%.2f",getRerata());
    }

}
