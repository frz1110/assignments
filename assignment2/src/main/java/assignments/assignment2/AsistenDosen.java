package assignments.assignment2;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class AsistenDosen {
    private List<Mahasiswa> listMahasiswa = new ArrayList<>();
    private String kode;
    private String nama;

    public AsistenDosen(String kode, String nama) {
        // constructor untuk AsistenDosen.
        this.kode = kode;
        this.nama = nama;
    }

    public String getKode() {
        // kembalikan kode AsistenDosen.
        return this.kode;
    }


    public void addMahasiswa(Mahasiswa mahasiswa) {
        // tambahkan mahasiswa ke dalam daftar mahasiswa dengan mempertahankan urutan NPM.
        // Memanfaatkan method compareTo pada class Mahasiswa.
        listMahasiswa.add(mahasiswa);
        Collections.sort(listMahasiswa);
    }

    public Mahasiswa getMahasiswa(String npm) {
        // kembalikan objek Mahasiswa dengan NPM tertentu dari daftar mahasiswa.
        // jika tidak ada, kembalikan null.
        for (Mahasiswa e:listMahasiswa){
            if (e.getNpm().equals(npm)){
                return e;
            }
        }
        return null;
    }

    public String rekap() {
        // rekap semua nilai mahasiswa.
        String rekapan ="";
        for ( Mahasiswa e : listMahasiswa){
            rekapan += e + "\n" + "--------------------" + "\n" + e.rekap() + "\n";
        }
        return rekapan;
    }

    public String toString() {
        //kembalikan representasi String dari AsistenDosen.
        return this.kode + " - " + this.nama;
    }
}
