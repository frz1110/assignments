package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public abstract class Carrier{

    private String nama;
    private String tipe;
    private Status statusCovid = new Negatif();
    private int aktifKasusDisebabkan;
    private int totalKasusDisebabkan;
    private List<Carrier> rantaiPenular = new ArrayList<Carrier>();;

    public Carrier(String nama,String tipe){
        //  constructor untuk Carrier.
        this.nama = nama;
        this.tipe = tipe;
    }

    public String getNama(){
        // Kembalikan nilai dari atribut nama
        return this.nama;
    }

    public String getTipe(){
        // Kembalikan nilai dari atribut tipe
        return this.tipe;
    }

    public String getStatusCovid(){
        //Kembalikan nilai dari atribut statusCovid
        return this.statusCovid.getStatus();
    }

    public int getAktifKasusDisebabkan(){
        // Kembalikan nilai dari atribut aktifKasusDisebabkan
        return this.aktifKasusDisebabkan;
    }

    public int getTotalKasusDisebabkan(){
        // Kembalikan nilai dari atribut totalKasusDisebabkan
        return this.totalKasusDisebabkan;
    }

    public int setTotalKasusDisebabkan(){
        // Kembalikan nilai dari atribut totalKasusDisebabkan
        return this.totalKasusDisebabkan ++;
    }

    public void setAktifKasusDisebabkan(Carrier lain){
        // Kembalikan nilai dari atribut totalKasusDisebabkan
        if (lain.getStatusCovid().equals("Positif"))
            this.aktifKasusDisebabkan ++;
        else
            this.aktifKasusDisebabkan --;
    }

    

    public List<Carrier> getRantaiPenular(){
        // Kembalikan nilai dari atribut rantaiPenular
        return rantaiPenular;
    }

    public void ubahStatus(String status){
        // mengubah atribut dari statusCovid
        if(status.equalsIgnoreCase("positif"))
            this.statusCovid = new Positif();
        else if(status.equalsIgnoreCase("Negatif"))
            this.statusCovid = new Negatif();
      
    }

    public void interaksi(Carrier lain){
        // Objek ini berinteraksi dengan objek lain
        this.statusCovid.tularkan(this, lain);

    }

    public abstract String toString();

}
