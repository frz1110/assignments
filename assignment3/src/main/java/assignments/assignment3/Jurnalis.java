package assignments.assignment3;

public class Jurnalis extends Manusia{
  	
    public Jurnalis(String name){
        //constructor untuk Jurnalis.
        super(name);
    }

    public String toString(){
        //overide method toString()
        return "JURNALIS " + getNama();
    }
}