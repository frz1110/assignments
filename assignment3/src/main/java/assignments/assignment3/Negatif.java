package assignments.assignment3;

public class Negatif implements Status{
  
    public String getStatus(){
        return "Negatif";
    }

    public void tularkan(Carrier penular, Carrier tertular){
        boolean ubah = false;
        if(tertular.getStatusCovid().equals("Positif")){
            // Interaksi manusia dan manusia
            if(tertular instanceof Manusia && penular instanceof Manusia){
                penular.ubahStatus("Positif");
                ubah = true;
            }
            //Interaksi manusia (positif) dan benda
            else if ( tertular instanceof Manusia && penular instanceof Benda){
                ((Benda)penular).tambahPersentase();
                if ( ((Benda)penular).getPersentaseMenular() >= 100){
                    penular.ubahStatus("Positif");
                    ubah = true;
                }
            }
             // Interaksi Benda (positif) dengan manusia.
            else if (penular instanceof Manusia && tertular instanceof Benda){
                penular.ubahStatus("Positif");
                ubah = true;

            }
            
            // Membuat rantai penular
            //menambah total aktif dan total kasus yang disebabkan suatu objek.
            if (ubah){
                for (Carrier e:tertular.getRantaiPenular()){
                    if( !penular.getRantaiPenular().contains(e))
                        penular.getRantaiPenular().add(e);

                    if( !penular.getNama().equals(e.getNama())){
                        e.setTotalKasusDisebabkan();
                        e.setAktifKasusDisebabkan(penular);
                    }
                }

                penular.getRantaiPenular().add(tertular);
                if(!tertular.getNama().equals(penular.getNama())){
                    tertular.setTotalKasusDisebabkan();
                    tertular.setAktifKasusDisebabkan(penular);
                }
            }
        
        }
    }
}
