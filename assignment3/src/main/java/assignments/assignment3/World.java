package assignments.assignment3;

import java.util.ArrayList;
import java.util.List;

public class World{

    public List<Carrier> listCarrier;

    public World(){
        // constructor untuk class World
        listCarrier = new ArrayList<Carrier>();
    }

    public Carrier createObject(String tipe, String nama){
        // membuat object sesuai dengan parameter yang diberikan
        Carrier carrier = null;
        if (tipe.equalsIgnoreCase("jurnalis")){
            carrier = new Jurnalis(nama);
        }
        else if (tipe.equalsIgnoreCase("cleaning_service")){
            carrier = new CleaningService(nama);
        }
        else if (tipe.equalsIgnoreCase("ojol")){
            carrier = new Ojol(nama);
        }
        else if (tipe.equalsIgnoreCase("pekerja_jasa")){
            carrier = new PekerjaJasa(nama);
        }
        else if (tipe.equalsIgnoreCase("Petugas_medis")){
            carrier = new PetugasMedis(nama);
        }
        else if (tipe.equalsIgnoreCase("Pintu")){
            carrier = new Pintu(nama);
        }
        else if (tipe.equalsIgnoreCase("tombol_lift")){
            carrier = new TombolLift(nama);
        }
        else if (tipe.equalsIgnoreCase("angkutan_umum")){
            carrier = new AngkutanUmum(nama);
        }
        else if (tipe.equalsIgnoreCase("pegangan_tangga")){
            carrier = new PeganganTangga(nama);
        }
        return carrier;
    }

    public Carrier getCarrier(String nama){
        //  mengambil object carrier dengan nama sesuai dengan parameter
        Carrier carrier = null;
        for (Carrier e:listCarrier){
            if (e.getNama().equalsIgnoreCase(nama)){
                carrier = e;
            }
        }
        return carrier;
    }
}
