package assignments.assignment3;

public class CleaningService extends Manusia{
  		
    private int jumlahDibersihkan;

    public CleaningService(String nama){
        // Buat constructor untuk CleaningService.
        // Akses constructor superclass-nya
        super(nama);
    }

     /* Mengubah status benda dari positif menjadi negatif dan me-reset presentase menular menjadi 0.
        Jika benda masih negatif maka hanya di reset menjadi 0. */
    public void bersihkan(Benda benda){
        
        if(benda.getStatusCovid().equalsIgnoreCase("positif")){
            benda.ubahStatus("Negatif");
            //Mengupdate kasus aktif yang disebabkan suatu objek (berkurang).
            for (Carrier e:benda.getRantaiPenular()){
                if(!benda.getNama().equals(e.getNama()))
                    e.setAktifKasusDisebabkan(benda);
            }
        }
        benda.setPersentaseMenular(0);
        jumlahDibersihkan ++;
    }

    public int getJumlahDibersihkan(){
        // Kembalikan nilai dari atribut jumlahDibersihkan
        return this.jumlahDibersihkan;
    }

    public String toString(){
         //overide method toString().
        return "CLEANING SERVICE " + getNama();
    }

}