package assignments.assignment3;

public class PeganganTangga extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda
      
    public PeganganTangga(String name){
        //  Buat constructor untuk Jurnalis.
        //  Akses constructor superclass-nya
        super(name);
    }

    public void tambahPersentase(){
        //Menambah presentase menular.
        persentaseMenular += 20;
    }

    public String toString(){
        //overide method toString()
        return "PEGANGAN TANGGA " + getNama();
    }
    
}