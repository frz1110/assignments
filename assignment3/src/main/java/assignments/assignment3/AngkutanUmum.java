package assignments.assignment3;

public class AngkutanUmum extends Benda{
      
    public AngkutanUmum(String name){
    // Buat constructor untuk AngkutanUmum.
        super(name);
    }

    public void tambahPersentase(){
    // tambah presentase menular
        persentaseMenular += 35;
    }

    public String toString(){
    //overide method toString().
        return "ANGKUTAN UMUM " + getNama();
    }

}