package assignments.assignment3;

public class PekerjaJasa extends Manusia{
  	
    public PekerjaJasa(String nama){
    	// Buat constructor untuk Jurnalis.
        // Akses constructor superclass-nya
        super(nama);
    }

    public String toString(){
        //overide method toString()
        return "PEKERJA JASA " + getNama();
    }
  	
}