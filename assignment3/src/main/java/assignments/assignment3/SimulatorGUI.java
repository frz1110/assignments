package assignments.assignment3;

import java.io.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.collections.FXCollections;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.geometry.Pos;
import javafx.geometry.Insets;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class SimulatorGUI extends Application {
    private StringWriter pw = new StringWriter();
    private GUI gui = new GUI();
    
    @Override
    public void start(Stage stage) {
        
        String[] query = {"ADD", "POSITIFKAN","INTERAKSI","RANTAI","SEMBUHKAN" ,"BERSIHKAN","EXIT","AKTIF KASUS DARI OBJEK", "TOTAL KASUS DARI OBJEK", "TOTAL SEMBUH PETUGAS MEDIS", "TOTAL BERSIH CLEANING SERVICE",  "TOTAL SEMBUH MANUSIA"};
        Button[] btLst = new Button[12];
        HBox bt1 = new HBox();
        HBox bt2 = new HBox();
        BorderPane button = new BorderPane();
        TextArea area = new TextArea();
        Button ok = new Button("   OK   ");

        // Membuat button.
        for(int i=0; i < 7; i++){
            Button bt = new Button(query[i]);
            bt.setPrefWidth(180);
            btLst[i] = bt;
            bt1.getChildren().add(bt);
        }

        for(int i=7; i < 12; i++){
            Button bt = new Button(query[i]);
            bt.setPrefWidth(180);
            btLst[i] = bt;
            bt2.getChildren().add(bt);
        }

        //Mengatur Layout halaman utama.
        button.setTop(bt1);
        button.setBottom(bt2);
        button.setCenter(new Label("Selamat datang di program simulasi COVID-19!"));

        // Menjalankan query ketika button diklik
        btLst[0].setOnAction(e -> {

            VBox vBox = new VBox(10);
            HBox hbox1 = new HBox(10);
            HBox hbox2 = new HBox(10);
            HBox hbox3 = new HBox(ok);
            TextField nama = new TextField();

            String[] tipe = {"JURNALIS", "OJOL","PEKERJA_JASA","PETUGAS_MEDIS","CLEANING_SERVICE" ,"ANGKUTAN_UMUM","PINTU", "TOMBOL_LIFT", "PEGANGAN_TANGGA"};
            ChoiceBox choice = new ChoiceBox(FXCollections.observableArrayList(tipe));

            hbox1.getChildren().addAll(new Label("Tipe: "), choice);
            hbox2.getChildren().addAll(new Label("Nama: "), nama);
            hbox3.setPadding(new Insets(0,0,0,140));
            vBox.getChildren().addAll(hbox1,hbox2, hbox3);

            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            hbox3.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);

            ok.setOnAction(x ->{
                gui.querys("ADD", (String)choice.getValue() + " " + nama.getText(), pw);
                nama.clear();
            });
        });

        
        btLst[1].setOnAction(e -> {
            VBox vBox = new VBox(10);
            HBox hbox1= new HBox(10);
            HBox hbox2= new HBox(ok);
            TextField nama = new TextField();

            hbox1.getChildren().addAll(new Label("Nama: "), nama);
            vBox.getChildren().addAll(hbox1, hbox2);

            hbox2.setPadding(new Insets(0,0,0,140));
            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);
            ok.setOnAction(x ->{
                gui.querys("POSITIFKAN", nama.getText(), pw);
                nama.clear(); 
            });
        });

        btLst[2].setOnAction(e -> {
            VBox vBox = new VBox(10);
            HBox hbox1= new HBox(10);
            HBox hbox2= new HBox(10);
            HBox hbox3= new HBox(ok);
            TextField nama1 = new TextField();
            TextField nama2 = new TextField();

            hbox1.getChildren().addAll(new Label("Nama 1: "), nama1);
            hbox2.getChildren().addAll(new Label("Nama 2: "), nama2);
            vBox.getChildren().addAll(hbox1, hbox2, hbox3);

            hbox3.setPadding(new Insets(0,0,0,140));

            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            hbox3.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);
            ok.setOnAction(x ->{
                gui.querys("INTERAKSI", nama1.getText() + " " + nama2.getText(), pw);
                nama1.clear();
                nama2.clear();
            });
        
        });

        btLst[3].setOnAction(e -> {
            VBox vBox = new VBox(10);
            HBox hbox1= new HBox(10);
            HBox hbox2= new HBox(ok);
            TextField nama = new TextField();

            hbox1.getChildren().addAll(new Label("Nama: "), nama);
            vBox.getChildren().addAll(hbox1, hbox2);

            hbox2.setPadding(new Insets(0,0,0,140));
            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);
            ok.setOnAction(x ->{
                gui.querys("RANTAI", nama.getText(), pw);
                nama.clear();
            });    
        });
        btLst[4].setOnAction(e -> {
            VBox vBox = new VBox(10);
            HBox hbox1= new HBox(10);
            HBox hbox2= new HBox(10);
            HBox hbox3= new HBox(ok);
            TextField nama1 = new TextField();
            TextField nama2 = new TextField();

            hbox1.getChildren().addAll(new Label("Nama 1: "), nama1);
            hbox2.getChildren().addAll(new Label("Nama 2: "), nama2);
            vBox.getChildren().addAll(hbox1, hbox2, hbox3);

            hbox3.setPadding(new Insets(0,0,0,140));
            
            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            hbox3.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);
            ok.setOnAction(x ->{
                gui.querys("SEMBUHKAN", nama1.getText() + " " + nama2.getText(), pw);
                nama1.clear();
                nama2.clear();
            });
        });

        btLst[5].setOnAction(e -> {
            VBox vBox = new VBox(10);
            HBox hbox1= new HBox(10);
            HBox hbox2= new HBox(10);
            HBox hbox3= new HBox(ok);
            TextField nama1 = new TextField();
            TextField nama2 = new TextField();

            hbox1.getChildren().addAll(new Label("Nama 1: "), nama1);
            hbox2.getChildren().addAll(new Label("Nama 2: "), nama2);
            vBox.getChildren().addAll(hbox1, hbox2, hbox3);

            hbox3.setPadding(new Insets(0,0,0,140));
            
            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            hbox3.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);
            ok.setOnAction(x ->{
                gui.querys("BERSIHKAN", nama1.getText() + " " + nama2.getText(), pw);
                nama1.clear();
                nama2.clear();
            });
        });

        btLst[6].setOnAction(e ->{
            //Menampilkan Output.
            button.getChildren().clear();
            area.setText(pw.toString());
            button.setCenter(area);
        });

        btLst[7].setOnAction(e -> {
            VBox vBox = new VBox(10);
            HBox hbox1= new HBox(10);
            HBox hbox2= new HBox(ok);
            TextField nama = new TextField();

            hbox1.getChildren().addAll(new Label("Nama: "), nama);
            vBox.getChildren().addAll(hbox1, hbox2);

            hbox2.setPadding(new Insets(0,0,0,140));
            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);
            ok.setOnAction(x ->{
                gui.querys("AKTIF KASUS DARI OBJEK", nama.getText(), pw);
                nama.clear();
            });
        });

        btLst[8].setOnAction(e -> {
            VBox vBox = new VBox(10);
            HBox hbox1= new HBox(10);
            HBox hbox2= new HBox(ok);
            TextField nama = new TextField();

            hbox1.getChildren().addAll(new Label("Nama: "), nama);
            vBox.getChildren().addAll(hbox1, hbox2);

            hbox2.setPadding(new Insets(0,0,0,140));
            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);
            ok.setOnAction(x ->{
                gui.querys("TOTAL KASUS DARI OBJEK", nama.getText(), pw);
                nama.clear();
            });
        });
        btLst[9].setOnAction(e -> {
            VBox vBox = new VBox(10);
            HBox hbox1= new HBox(10);
            HBox hbox2= new HBox(ok);
            TextField nama = new TextField();

            hbox1.getChildren().addAll(new Label("Nama: "), nama);
            vBox.getChildren().addAll(hbox1, hbox2);

            hbox2.setPadding(new Insets(0,0,0,140));
            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);
            ok.setOnAction(x ->{
                gui.querys("TOTAL SEMBUH PETUGAS MEDIS", nama.getText(), pw);
                nama.clear();
            });
        });

        btLst[10].setOnAction(e -> {
            VBox vBox = new VBox(10);
            HBox hbox1= new HBox(10);
            HBox hbox2= new HBox(ok);
            TextField nama = new TextField();

            hbox1.getChildren().addAll(new Label("Nama: "), nama);
            vBox.getChildren().addAll(hbox1, hbox2);

            hbox2.setPadding(new Insets(0,0,0,140));
            hbox1.setAlignment(Pos.CENTER);
            hbox2.setAlignment(Pos.CENTER);
            vBox.setAlignment(Pos.CENTER);

            button.setCenter(vBox);
            ok.setOnAction(x ->{
                gui.querys("TOTAL BERSIH CLEANING SERVICE", nama.getText(), pw);
                nama.clear();
            });
        });
        btLst[11].setOnAction(e -> {
            gui.querys("TOTAL SEMBUH MANUSIA", "" , pw);
            button.setCenter(new Label("Total Sembuh Berhasil di Dapatkan"));
        });

        Scene scene = new Scene(button, 900, 350);
        stage.setScene(scene);
        stage.setTitle("Simulasi Covid-19");
        stage.show();

        }

    public static void main(String[] args) {
        launch();
    }

}

class GUI{
    private World world;

    public GUI(){
        world = new World(); 
    }
    

    public void querys(String query, String text, StringWriter pw) {
        String[] str = text.split(" ");
        int totalSembuh = 0;
        if (query.equalsIgnoreCase("ADD")){
            world.listCarrier.add(world.createObject(str[0], str[1])); 
        }
        //Melakukan interaksi antar objek carrier
        else if (query.equalsIgnoreCase("INTERAKSI")){
            world.getCarrier(str[0]).interaksi(world.getCarrier(str[1]));
            
        }//Mengubah status COVID menjadi positif.
        else if (query.equalsIgnoreCase("POSITIFKAN")){
            (world.getCarrier(str[0])).ubahStatus("Positif");
            System.out.println("hola");
        }
        //Mengubah status COVID(Manusia) menjadi negatif.
        else if (query.equalsIgnoreCase("SEMBUHKAN")){
            ((PetugasMedis)world.getCarrier(str[0])).obati((Manusia)world.getCarrier(str[1]));
            totalSembuh ++;
        }
        //Mengubah status COVID(Benda) menjadi negatif.
        else if (query.equalsIgnoreCase("BERSIHKAN")){
            ((CleaningService)world.getCarrier(str[0])).bersihkan((Benda)world.getCarrier(str[1]));
        }
        // Menampilkan rantai penularan objek. Terdapat exception jika objek masih negatif
        else if (query.equalsIgnoreCase("RANTAI")){
            try{
            if (world.getCarrier(str[0]).getStatusCovid().equalsIgnoreCase("Positif")){
                pw.write( String.format("Rantai Penyebaran %s: ",world.getCarrier(str[0])));
                for ( Carrier e: world.getCarrier(str[0]).getRantaiPenular()){   
                    pw.write(e.toString()+ " -> ");
                }
                pw.write(world.getCarrier(str[0]).toString()+"\n");
                }
            else{
                throw new BelumTertularException(world.getCarrier(str[0]).toString() + " berstatus negatif");
                }
            }
            catch(BelumTertularException e){
                pw.write("assignments.assignment3.BelumTertularException: " + e.getMessage() + "\n");
            }
        
        }
        // Menampilkan banyak kasus yang disebabkan oleh objek (baik masih positif maupun sudah sembuh).
        else if (query.equals("TOTAL KASUS DARI OBJEK")){
            pw.write(String.format("%s telah menyebarkan virus COVID ke %d objek\n", world.getCarrier(str[0]),world.getCarrier(str[0]).getTotalKasusDisebabkan()));
        }
        // Menampilkan banyak kasus yang masih positif yang disebabkan oleh objek.
        else if (query.equals("AKTIF KASUS DARI OBJEK")){
            pw.write(String.format("%s telah menyebarkan virus COVID dan masih terindefikasi positif sebanyak %d objek\n", world.getCarrier(str[0]),world.getCarrier(str[0]).getAktifKasusDisebabkan()));
        }
        // Menampilkan total manusia yang sudah sembuh.
        else if (query.equalsIgnoreCase("TOTAL SEMBUH MANUSIA")){
            pw.write("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: " + totalSembuh + " kasus" + "\n");
        }
        // Menampilkan banyak manusia yang berhasil diobati oleh seorang petugas medis.
        else if (query.equalsIgnoreCase("TOTAL SEMBUH PETUGAS MEDIS")){
            pw.write((PetugasMedis)world.getCarrier(str[0]) + " Menyembuhkan " + ((PetugasMedis)world.getCarrier(str[0])).getJumlahDisembuhkan()+" Manusia" + "\n");
        }
        // Menampilkan banyak benda yang berhasil dibersihkan oleh seorang petugas medis.
        else if (query.equalsIgnoreCase("TOTAL BERSIH CLEANING SERVICE")){
            pw.write((CleaningService)world.getCarrier(str[0]) + " Membersihkan " +((CleaningService)world.getCarrier(str[0])).getJumlahDibersihkan()+" Benda" + "\n");
        }
       
    }
    

}

