package assignments.assignment3;

public class PetugasMedis extends Manusia{
  		
    private int jumlahDisembuhkan;

    public PetugasMedis(String nama){
        // Buat constructor untuk Jurnalis.
        // Akses constructor superclass-nya
        super(nama);
    }

    public void obati(Manusia manusia) {
        //  objek PetugasMedis ini menyembuhkan manusia
        manusia.ubahStatus("Negatif");
        //Mengupdate kasus aktif yang disebabkan suatu objek (berkurang).
        for (Carrier e:manusia.getRantaiPenular()){
            if(!manusia.getNama().equals(e.getNama()))
                e.setAktifKasusDisebabkan(manusia);
        }
        this.jumlahDisembuhkan++;
    }

    public int getJumlahDisembuhkan(){
        // TODO: Kembalikan nilai dari atribut jumlahDisembuhkan
        return this.jumlahDisembuhkan;
    }

    public String toString(){
        return "PETUGAS MEDIS " + getNama();
    }
}