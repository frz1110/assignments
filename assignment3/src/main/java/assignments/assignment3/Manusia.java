package assignments.assignment3;

public abstract class Manusia extends Carrier{
    private static int jumlahSembuh = 0;
    
    public Manusia(String nama){
        // constructor untuk Manusia.
        super(nama,"Manusia");
    }
    
    public void tambahSembuh(){
        // Fungsi untuk menambahkan nilai pada atribut jumlahSembuh.
        jumlahSembuh ++;
    }

    public static int getJumlahSembuh() {
        // Kembalikan nilai untuk atribut jumlahSembuh.
        return jumlahSembuh;
    }
    
}