package assignments.assignment3;

public class Positif implements Status{
  
    public String getStatus(){
        return "Positif";
    }

    public void tularkan(Carrier penular, Carrier tertular){
        boolean ubah = false;
        //Penular yang positif menularkan kepada tertular yang negatif.
        if(tertular.getStatusCovid().equals("Negatif")){
            //Interaksi manusia dengan manusia
            if(penular instanceof Manusia && tertular instanceof Manusia){
                tertular.ubahStatus("Positif");
                ubah = true;
            }
            //Interaksi manusia (positif) dengan benda
            else if( penular instanceof Manusia && tertular instanceof Benda ){
                ((Benda)tertular).tambahPersentase();
                if( ((Benda)tertular).getPersentaseMenular() >= 100){
                    tertular.ubahStatus("Positif");
                    ubah = true;
                    }
            }
            // Interaksi benda (positif) dengan manusia
            else if(penular instanceof Benda && tertular instanceof Manusia){
                tertular.ubahStatus("Positif");
                ubah = true;   
            }
            // Membuat rantai penular
            //menambah total aktif dan total kasus yang disebabkan suatu objek.
            if ( ubah ){
                for (Carrier e:penular.getRantaiPenular()){
                    if( !tertular.getRantaiPenular().contains(e))
                        tertular.getRantaiPenular().add(e);

                    if(!tertular.getNama().equals(e.getNama())){
                        e.setTotalKasusDisebabkan();
                        e.setAktifKasusDisebabkan(tertular);
                    }
                }

                tertular.getRantaiPenular().add(penular);
                if(!tertular.getNama().equals(penular.getNama())){
                    penular.setTotalKasusDisebabkan();
                    penular.setAktifKasusDisebabkan(tertular);
                }
            }
        }
    }
} 