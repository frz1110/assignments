package assignments.assignment3;


public abstract class Benda extends Carrier{
  
    protected int persentaseMenular = 0;

    public Benda(String name){
        // Buat constructor untuk Benda.
        // Akses constructor superclass-nya
        super(name, "Benda");
    }

    public abstract void tambahPersentase();

    public int getPersentaseMenular(){
        // Kembalikan nilai dari atribut persentaseMenular
        return this.persentaseMenular;
    }
    
    public void setPersentaseMenular(int persentase) {
        // Gunakan sebagai setter untuk atribut persentase menular
        this.persentaseMenular = persentase;
    }
}