package assignments.assignment3;

public class Pintu extends Benda{
    // TODO: Implementasikan abstract method yang terdapat pada class Benda  
    
    public Pintu(String name){
        // TODO: Implementasikan apabila objek CleaningService ini membersihkan benda
        // Hint: Update nilai atribut jumlahDibersihkan
        super(name);
    }
    public void tambahPersentase(){
        // Menambah presentase menular.
       persentaseMenular += 30;
    }

    public String toString(){
        //Overide method toString().
        return "PINTU " + getNama();
    }
}