package assignments.assignment3;

import java.io.*;
import java.lang.*;

public class InputOutput{
  	
    private BufferedReader br;
    private PrintWriter pw;
    private String inputFile;
    private String outputFile;
    private String inputType;
    private String outputType;
    private World world;

    public InputOutput(String inputType, String inputFile, String outputType, String outputFile) {
        // constructor untuk InputOutput.
        this.inputFile = inputFile;
        this.outputFile = outputFile;
        this.inputType = inputType;
        this.outputType = outputType;
        world = new World();
    }

    public void setBufferedReader(String inputType) throws IOException{
        // Membuat BufferedReader bergantung inputType (I/O text atau input terminal) 
        
        if ( inputType.equalsIgnoreCase("terminal")){
            this.br = new BufferedReader(new InputStreamReader(System.in));
        }
        else if ( inputType.equalsIgnoreCase("text")){
            this.br = new BufferedReader(new FileReader(this.inputFile));
        }

    }
    
    public void setPrintWriter(String outputType) throws IOException{
        // Membuat PrintWriter bergantung inputType (I/O text atau output terminal) 

        if ( outputType.equalsIgnoreCase("terminal")){
            this.pw = new PrintWriter(new OutputStreamWriter(System.out));
        }
        else if ( outputType.equalsIgnoreCase("text")){
            this.pw = new PrintWriter(new File(this.outputFile));
        }
        
    }

    public void BelumTertular(String carrier) throws BelumTertularException{
        // method untuk exception
        throw new BelumTertularException(carrier + " berstatus negatif");
    }

    public void run() throws IOException{
        // Program utama untuk InputOutput.
       
        try{
            
        setBufferedReader(this.inputType);
        setPrintWriter(this.outputType);
        int totalSembuh = 0;
        String baris = this.br.readLine();

        while (baris  != null){
            String[] str = baris.split(" ", 3);
            String query = str[0];
            //Membuat objek carrier dan memasukkannya ke list
            if (query.equalsIgnoreCase("ADD")){
                world.listCarrier.add(world.createObject(str[1], str[2]));
            }
            //Melakukan interaksi antar objek carrier
            else if (query.equalsIgnoreCase("INTERAKSI")){
                world.getCarrier(str[1]).interaksi(world.getCarrier(str[2]));
            }
            //Mengubah status COVID menjadi positif.
            else if (query.equalsIgnoreCase("POSITIFKAN")){
                (world.getCarrier(str[1])).ubahStatus("Positif");
            }
            //Mengubah status COVID(Manusia) menjadi negatif.
            else if (query.equalsIgnoreCase("SEMBUHKAN")){
                ((PetugasMedis)world.getCarrier(str[1])).obati((Manusia)world.getCarrier(str[2]));
                totalSembuh ++;
            }
            //Mengubah status COVID(Benda) menjadi negatif.
            else if (query.equalsIgnoreCase("BERSIHKAN")){
                ((CleaningService)world.getCarrier(str[1])).bersihkan((Benda)world.getCarrier(str[2]));
            }
            // Menampilkan rantai penularan objek. Terdapat exception jika objek masih negatif
            else if (query.equalsIgnoreCase("RANTAI")){
                if (world.getCarrier(str[1]).getStatusCovid().equalsIgnoreCase("Positif")){
                    pw.print( String.format("Rantai penyebaran %s: ",world.getCarrier(str[1])));
                    for ( Carrier e: world.getCarrier(str[1]).getRantaiPenular()){
                        pw.print(e.toString()+ " -> ");
                    }
                    pw.println(world.getCarrier(str[1]));
                }
                else{
                    try{
                        BelumTertular(world.getCarrier(str[1]).toString());
                    }catch(BelumTertularException e){
                        pw.println(e);
                    }
                }    
            }
            // Menampilkan banyak kasus yang disebabkan oleh objek (baik masih positif maupun sudah sembuh).
            else if (query.equals("TOTAL_KASUS_DARI_OBJEK")){
                pw.println(String.format("%s telah menyebarkan virus COVID ke %d objek", world.getCarrier(str[1]),world.getCarrier(str[1]).getTotalKasusDisebabkan()));
            }
            // Menampilkan banyak kasus yang masih positif yang disebabkan oleh objek.
            else if (query.equals("AKTIF_KASUS_DARI_OBJEK")){
                pw.println(String.format("%s telah menyebarkan virus COVID dan masih teridentifikasi positif sebanyak %d objek", world.getCarrier(str[1]),world.getCarrier(str[1]).getAktifKasusDisebabkan()));
            }
            // Menampilkan total manusia yang sudah sembuh.
            else if (query.equalsIgnoreCase("TOTAL_SEMBUH_MANUSIA")){
                pw.println("Total sembuh dari kasus COVID yang menimpa manusia ada sebanyak: " + totalSembuh + " kasus");
            }
            // Menampilkan banyak manusia yang berhasil diobati oleh seorang petugas medis.
            else if (query.equalsIgnoreCase("TOTAL_SEMBUH_PETUGAS_MEDIS")){
                pw.println((PetugasMedis)world.getCarrier(str[1]) + " menyembuhkan " + ((PetugasMedis)world.getCarrier(str[1])).getJumlahDisembuhkan()+" manusia");
            }
            // Menampilkan banyak benda yang berhasil dibersihkan oleh seorang petugas medis.
            else if (query.equalsIgnoreCase("TOTAL_BERSIH_CLEANING_SERVICE")){
                pw.println((CleaningService)world.getCarrier(str[1]) + " membersihkan " +((CleaningService)world.getCarrier(str[1])).getJumlahDibersihkan()+" benda");
            }
            //Memberhentikan program
            if (query.equalsIgnoreCase("EXIT"))
                break;
            // pindah ke baris selanjutnya
            baris = this.br.readLine();
        }
        // close pw dan br
        this.br.close();
        this.pw.close();
        }
        // IOException jika file yang hendak dibaca tidak ditemukan.
        catch(IOException e){
            System.out.println("File tidak ditemukan");
        }
        


    }
    
}