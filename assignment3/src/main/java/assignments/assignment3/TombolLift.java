package assignments.assignment3;

public class TombolLift extends Benda{
      // TODO: Implementasikan abstract method yang terdapat pada class Benda
      
    public TombolLift(String name){
        // TODO: Buat constructor untuk Jurnalis.
        // Hint: Akses constructor superclass-nya
        super(name);
    }
    public void tambahPersentase(){
        //Menambah presentase menular
        persentaseMenular += 20;
    }

    public String toString(){
        //Override method toString().
        return "TOMBOL LIFT " + getNama();
    }
}