package assignments.assignment1;

import java.util.Scanner;

public class HammingCode {
    static final int ENCODE_NUM = 1;
    static final int DECODE_NUM = 2;
    static final int EXIT_NUM = 3;

    /**
     * Implementasi method encode.
     */
    public static String encode(String code) {
        int n = code.length();
        String newCode = "0";
        int parity = 2;
        int i = 0;
        double k = 1;
        //Menambahkan parity bit(tambah dengan nol dulu)
        while (n != 0) {
            if (Math.pow(parity,1 / k) == 2) { //Mengecek apakah ini posisi parity atau tidak
                newCode += 0;//tambah parity
                k++;
            } else {
                newCode += code.charAt(i++);//tambah kode awal
                n--;
            } 
            parity++;
        }
        int p = 1;
        double m = 0;
        //Cek even parity
        for (int j = 1;j <= newCode.length();j++) {
            int cekEven = 0;
            if (j == 1 || Math.pow(j,1 / m) == 2) {
                //cek even parity posisi 1,2,4,8,....      
                for (int a = j - 1;a < newCode.length();a += Math.pow(2,p)) {
                    //cek per grup
                    for (int b = a;b <= a + j - 1;++b) {
                        if (b < newCode.length()) {
                            cekEven += Integer.parseInt("" + newCode.charAt(b));
                        }
                    }
                } 
                if (cekEven % 2 == 1) { //Mengubah 0 menjadi 1 jika ganjil
                    newCode = newCode.substring(0,j - 1) + "1" + newCode.substring(j);
                } 
                m++; 
                p++;
            }  
        }
        return newCode;
    }
    
    /**
     * Implementasi method decode.
     */
    public static String decode(String code) {
        int p = 1;
        double m = 0;
        int error = 0;
        //Cek error di posisi 1,2,4,8.....(even parity)
        for (int j = 1;j <= code.length();j++) {
            int cekEven = 0;
            if (j == 1 || Math.pow(j,1 / m) == 2) {
                for (int a = j - 1;a < code.length();a += Math.pow(2,p)) {
                    for (int b = a;b <= a + j - 1;++b) { 
                        if (b < code.length()) {
                            cekEven += Integer.parseInt("" + code.charAt(b));//Menjumlahkan bit
                        }
                    }
                } 
                //Menghitung posisi error
                if (cekEven % 2 == 1) {
                    error += j;
                } 
                m++;  
                p++;
            }  
        }
        //Mengoreksi Error
        StringBuilder errorCorection = new StringBuilder(code);
        if (error != 0) {
            if (code.charAt(error - 1) == '0') {
                errorCorection.setCharAt(error - 1,'1');
            } else {
                errorCorection.setCharAt(error - 1,'0');
            }
        }
        //Menghilangkan Parity
        String corect = errorCorection.toString();
        String newCode = "";
        double k = 2;
        for (int j = 3;j <= code.length();j++) {
            if (Math.pow(j,1 / k) != 2) { 
                newCode += corect.charAt(j - 1);
            } else {
                k++;
            }
        }
        return newCode; 
    }

    /**
     * Main program for Hamming Code.
     * @param args unused
     */
    public static void main(String[] args) {
        System.out.println("Selamat datang di program Hamming Code!");
        System.out.println("=======================================");
        Scanner in = new Scanner(System.in);
        boolean hasChosenExit = false;
        while (!hasChosenExit) {
            System.out.println();
            System.out.println("Pilih operasi:");
            System.out.println("1. Encode");
            System.out.println("2. Decode");
            System.out.println("3. Exit");
            System.out.println("Masukkan nomor operasi yang diinginkan: ");
            int operation = in.nextInt();
            if (operation == ENCODE_NUM) {
                System.out.println("Masukkan data: ");
                String data = in.next();
                String code = encode(data);
                System.out.println("Code dari data tersebut adalah: " + code);
            } else if (operation == DECODE_NUM) {
                System.out.println("Masukkan code: ");
                String code = in.next();
                String data = decode(code);
                System.out.println("Data dari code tersebut adalah: " + data);
            } else if (operation == EXIT_NUM) {
                System.out.println("Sampai jumpa!");
                hasChosenExit = true;
            }
        }
        in.close();
    }
}